package main

import (
	"context"
	"fmt"

	"net/http"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/swarm"
	"github.com/docker/docker/client"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
)

var log = logrus.New()

type resourceCollector struct {
	cli                *client.Client
	cpuAvailable       *prometheus.Desc
	cpuReservations    *prometheus.Desc
	cpuLimits          *prometheus.Desc
	memoryAvailable    *prometheus.Desc
	memoryReservations *prometheus.Desc
	memoryLimits       *prometheus.Desc
}

func (c *resourceCollector) Describe(ch chan<- *prometheus.Desc) {
	ch <- c.cpuAvailable
	ch <- c.cpuReservations
	ch <- c.cpuLimits
	ch <- c.memoryAvailable
	ch <- c.memoryLimits
	ch <- c.memoryReservations
}

type resourceDescription struct {
	cpuReservation    float64
	cpuLimit          float64
	memoryLimit       float64
	memoryReservation float64
}

func (c *resourceCollector) Collect(ch chan<- prometheus.Metric) {
	ctx := context.Background()

	nodes, err := c.cli.NodeList(ctx, types.NodeListOptions{})
	if err != nil {
		log.Fatal(err)
		return
	}
	tasks, err := c.cli.TaskList(ctx, types.TaskListOptions{})
	if err != nil {
		log.Fatal(err)
		return
	}
	services, err := c.cli.ServiceList(ctx, types.ServiceListOptions{})
	if err != nil {
		log.Fatal(err)
		return
	}

	nodeMap := make(map[string]swarm.Node)
	for _, node := range nodes {
		nodeMap[node.ID] = node
		ch <- prometheus.MustNewConstMetric(c.cpuAvailable, prometheus.GaugeValue, float64(node.Description.Resources.NanoCPUs), node.Description.Hostname)
		ch <- prometheus.MustNewConstMetric(c.memoryAvailable, prometheus.GaugeValue, float64(node.Description.Resources.MemoryBytes), node.Description.Hostname)
	}

	serviceMap := make(map[string]swarm.Service)
	for _, service := range services {
		serviceMap[service.ID] = service
	}

	for _, task := range tasks {
		service := serviceMap[task.ServiceID]
		taskName := fmt.Sprintf("%s.%d", service.Spec.Name, task.Slot)

		node := nodeMap[task.NodeID]

		rd := getResourceDescription(task)

		ch <- prometheus.MustNewConstMetric(c.cpuReservations, prometheus.GaugeValue, rd.cpuReservation, task.ID, node.Description.Hostname, taskName, string(task.DesiredState))
		ch <- prometheus.MustNewConstMetric(c.cpuLimits, prometheus.GaugeValue, rd.cpuLimit, task.ID, node.Description.Hostname, taskName, string(task.DesiredState))
		ch <- prometheus.MustNewConstMetric(c.memoryReservations, prometheus.GaugeValue, rd.memoryReservation, task.ID, node.Description.Hostname, taskName, string(task.DesiredState))
		ch <- prometheus.MustNewConstMetric(c.memoryLimits, prometheus.GaugeValue, rd.memoryLimit, task.ID, node.Description.Hostname, taskName, string(task.DesiredState))
	}
}

func getResourceDescription(task swarm.Task) resourceDescription {
	rd := resourceDescription{}

	if task.Spec.Resources != nil {
		res := task.Spec.Resources
		if res.Limits != nil {
			rd.cpuLimit = float64(res.Limits.NanoCPUs)
			rd.memoryLimit = float64(res.Limits.MemoryBytes)
		}
		if res.Reservations != nil {
			rd.cpuReservation = float64(res.Reservations.NanoCPUs)
			rd.memoryReservation = float64(res.Reservations.MemoryBytes)
		}
	}

	return rd
}

const namespace = "swarm"
const resourceSubsystem = "resource"

func newResourceCollector(cli *client.Client) prometheus.Collector {
	return &resourceCollector{
		cli: cli,
		cpuAvailable: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "cpuAvailable"),
			"The available CPU on a swarm node (in nano cpus)",
			[]string{"node"},
			nil,
		),
		cpuReservations: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "cpuReservations"),
			"The CPU reservation for a task (in nano cpus)",
			[]string{"task_id", "node", "task_name", "task_state"},
			nil,
		),
		cpuLimits: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "cpuLimits"),
			"The CPU limit for a task (in nano cpus)",
			[]string{"task_id", "node", "task_name", "task_state"},
			nil,
		),
		memoryAvailable: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "memoryAvailable"),
			"The available Memory on a swarm node (in bytes)",
			[]string{"node"},
			nil,
		),
		memoryReservations: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "memoryReservations"),
			"The Memory reservation for a task (in bytes)",
			[]string{"task_id", "node", "task_name", "task_state"},
			nil,
		),
		memoryLimits: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, resourceSubsystem, "memoryLimits"),
			"The Memory limit for a task (in bytes)",
			[]string{"task_id", "node", "task_name", "task_state"},
			nil,
		),
	}
}

func getDockerClient(ctx context.Context) (*client.Client, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		log.Fatal("error when creating docker client: ", err)
		return nil, err
	}
	if _, err := cli.Ping(ctx); err != nil {
		log.Fatal("error when using docker client: ", err)
		return nil, err
	}
	return cli, nil
}

func main() {
	ctx := context.Background()

	cli, err := getDockerClient(ctx)
	if err != nil {
		log.Fatal("error when initializing docker connection: ", err)
		return
	}

	rc := newResourceCollector(cli)

	r := prometheus.NewRegistry()
	r.MustRegister(rc)

	http.Handle("/metrics", promhttp.HandlerFor(r, promhttp.HandlerOpts{}))

	if err := http.ListenAndServe("0.0.0.0:8080", nil); err != nil {
		log.Fatal(err)
	}
}
